
unit background;

{$mode objfpc}{$H+}{$J-}
interface

uses FGL, SDL2, vector2, SysUtils,cfg,texture_manager, glog, game_object, Classes;

type
  TBackground = class(TGameObject)
  public
    constructor Create;
    procedure Update;
      override;
    procedure Draw(pr: PSDL_Renderer);
      override;
  protected
    r, g, b, a: integer;
  end;

implementation


procedure TBackground.Draw(pr: PSDL_Renderer);

var
  x, y: integer;
  xi, yi: integer;
  xf, yf: integer;
begin

  SDL_SetRenderDrawColor(pr, r, g, b, a);
  y := (round(position.y) mod h) - h;
  while y <= WINDOW_HEIGHT do
  begin
    x := (round(position.x) mod w) - w;
    while x <= WINDOW_WIDTH do

    begin
      xi := round(x - position.x) div w;
      yi := round(y - position.y) div h;

      if (xi mod 2) <> 0 then
        xf := SDL_FLIP_HORIZONTAL;

      if (yi mod 2) <> 0 then
        yf := SDL_FLIP_VERTICAL;

      flip := xf or yf;
      TTextureManager.Instance().Draw(pr, textureId, x, y, w, h, 1.0, 0.0,
        flip);

      Inc(x, w);
    end;
    Inc(y, h);

  end;
end;

constructor TBackground.Create;
begin
  inherited Create;
  AnimationCounter := -1;

  Update;
end;

procedure TBackground.Update;

var
  frames, hue, intensity: integer;
begin
  Inc(self.animationCounter);
  a := 255;

  frames := round(2.5 * FPS);
  hue := (self.animationCounter div frames) mod 6;
  intensity := round(255 * abs(cos(self.animationCounter / (2 * frames) * pi)));

  case hue of
    0:
    begin
      r := 255;
      g := 0;
      b := intensity;
    end;
    1:
    begin
      r := 255;
      g := intensity;
      b := 0;
    end;
    2:
    begin
      r := intensity;
      g := 255;
      b := 0;
    end;
    3:
    begin
      r := 0;
      g := 255;
      b := intensity;
    end;
    4:
    begin
      r := 0;
      g := intensity;
      b := 255;
    end;
    5:
    begin
      r := intensity;
      g := 0;
      b := 255;
    end;
  end;

  position.x := cos(self.animationCounter / (FPS * 3)) * w;
  position.y := self.animationCounter / 2;
end;



end.
