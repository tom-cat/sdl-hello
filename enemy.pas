
unit enemy;

{$mode objfpc}{$H+}{$J-}
interface

uses FGL, SDL2, vector2, cfg, math, SysUtils, glog, game_object, Classes;

type
  TEnemy = class(TGameObject)
  public
    procedure Update();
      override;
  end;

implementation


procedure TEnemy.Update;
	var dx,mf :integer;

begin
  //self.currentFrame := round(SDL_GetTicks() / 100) mod 6;
  inherited Update;
  velocity.x := (250 * 2) / 38.188 * cos(pi * animationCounter / FPS);
  velocity.y := (200 * 2) / 38.188 * sin(pi * animationCounter / FPS);
  writeln(animationCounter, ',', velocity.toString);
  dx := round(velocity.x);

  if dx <0 then flip := SDL_FLIP_NONE
  else 
  if dx >0 then flip := SDL_FLIP_HORIZONTAL;

  currentFrame := round(animationCounter * animationFrames / FPS) mod animationFrames
end;

end.
