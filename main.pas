
program demo;

{$mode objfpc}{$H+}

uses
  SysUtils,
  game,
  cfg,
  glog,
  vector2,
  sdl2;

var
  g: TGame;
begin
  g := TGame.Create;
  g.Init('Hello', SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH,
    WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
  while g.isRunning do
  begin
    g.Tick();
  end;
  g.Free;
  exit;
end.
