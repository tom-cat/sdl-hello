unit cfg;

{$mode objfpc}{$H+}

interface

const
  FPS = 60;
  DELAY_TIME = 1000.0 / FPS;
  WINDOW_WIDTH = 800;
  WINDOW_HEIGHT = 600;
  PLAYER_SPEED = 10;

implementation

end.
