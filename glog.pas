unit glog;

{$mode objfpc}{$H+}

interface

uses SysUtils;

procedure WritelnLog(const category: string; const message: string); overload;
procedure WritelnLog(const message: string); overload;
procedure WritelnLog(const Category: string; const MessageBase: string;
  const Args: array of const); overload;
procedure WritelnLog(const MessageBase: string; const Args: array of const); overload;


implementation


function LogTimePrefixStr: string;
begin
  Result := FormatDateTime('yyyy"-"mm"-"dd" "tt.zz', Now()) + '> ';
end;

procedure WritelnLog(const Message: string);
begin
  writelnlog('', message);
end;

procedure WritelnLog(const Category: string; const Message: string);
var
  S: string;
begin
  S := LogTimePrefixStr;
  if Category <> '' then
    S := S + Category + ': ';
  S := S + Message;
  writeln(S);
end;

procedure WritelnLog(const Category: string; const MessageBase: string;
  const Args: array of const);
begin
  WritelnLog(Category, Format(MessageBase, Args));
end;

procedure WritelnLog(const MessageBase: string; const Args: array of const);
begin
  WritelnLog('', Format(MessageBase, Args));
end;

end.
