unit animated_game_object;

{$mode objfpc}{$H+}{$J-}

interface

uses FGL, SDL2, vector2, SysUtils, glog, texture_manager, game_object, Classes;

type
  TAnimatedGameObject = class(TGameObject)
  public
    AnimationSpeed: integer;
    //procedure Load(x, y, w1, h1: integer; id: string);override;
    procedure Update(); override;
  end;

implementation

{ TAnimatedGameObject }

(*
procedure TAnimatedGameObject.Load(x, y, w1, h1: integer; id: string);
begin
  inherited Load(x,y,w1,h1,id);
  (TTextureManager.Instance()).queryTexture(id, nil,nil, @AnimationFrames, nil);
  animationFrames := animationFrames div w1;
end;
*)
procedure TAnimatedGameObject.Update();
begin
  inherited Update;
  currentFrame := Round(AnimationCounter / (1000 / AnimationSpeed)) mod animationFrames;
end;

end.
