unit menu_button;

{$mode objfpc}{$H+}{$J-}

interface

uses FGL, SDL2, vector2, SysUtils, glog, input_handler, game_object, Classes;

type
  TButtonClickEvent = procedure of object;

  TMenuButton = class(TGameObject)
  public
    isReleased: boolean;
    OnClick: TButtonClickEvent;
    procedure Update; override;

  end;

implementation

procedure TMenuButton.Update;
var
  mp: TVector2;
begin
  currentFrame := Ord(MOUSE_OUT);
  mp := (TInputHandle.Instance()).GetMousePosition();
  if (mp.x < position.x + w) and (mp.x > position.x) and
  (mp.y < position.y + h) and (mp.y > position.y) then
  begin
    if not (TInputHandle.Instance()).isMouseButtonDown(Ord(MOUSE_LEFT)) then
    begin
      isReleased := True;
      currentFrame := Ord(MOUSE_OVER);
    end
    else
      if isReleased then
      begin
        writeln('mouse click');
        currentFrame := Ord(MOUSE_CLICKED);
        if Assigned(OnClick) then
          OnClick();

        isReleased := False;
      end;
  end
end;

end.
