
unit game_object;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, glog, cfg, Math, sdl2, input_handler, texture_manager,
  vector2;

type
  TButtonState = (MOUSE_OUT = 0, MOUSE_OVER = 1, MOUSE_CLICKED = 2);
  TMouseButton = (MOUSE_LEFT = 0, MOUSE_MIDDLE = 1, MOUSE_RIGHT = 2);

  TLoaderParam = class
  public
    position: TVector2;
    w, h: integer;
    textureId: string;
  end;

  TGameObject = class
  public
    constructor Create;
    procedure Load(x, y, w, h: integer; textureId: string);virtual;
    procedure Draw(pr: PSDL_Renderer);virtual;
    procedure Update();virtual;
    procedure Clean;virtual;
  public
    position: TVector2;
    velocity: TVector2;
    acceleration: TVector2;
    w, h: integer;
    currentFrame, currentRow, animationFrames: integer;
    textureId: string;
    flip: integer;
    angle, scale: single;
    animationCounter: int64;
  end;



  TSDLGameObject = class(TGameObject)

  end;


implementation


constructor TGameObject.Create;
begin
  inherited Create;
  scale := 1.0;
  angle := 0.0;
  position.x := 0;
  position.y := 0;

  velocity.x := 0;
  velocity.y := 0;

  acceleration.x := 0;
  acceleration.y := 0;

  animationCounter := 0;
end;




procedure TGameObject.Clean;
begin

end;

procedure TGameObject.Load(x, y, w, h: integer; textureId: string);

var
  Width: integer;
begin
  //  self.position := TVector2.Create;
  self.position.x := x;
  self.position.y := y;

  self.w := w;
  self.h := h;
  self.textureId := textureId;

  self.currentRow := 0;
  self.currentFrame := 0;

  TTextureManager.Instance().QueryTexture(textureId, nil, nil, @Width, nil);
  self.AnimationFrames := round(Width / self.w);
end;

procedure TGameObject.Draw(pr: PSDL_Renderer);
begin
  (*
  if velocity.IsZero then
    (TTextureManager.Instance()).Draw(pr, textureId, round(position.x),
      round(position.y), w, h, scale, angle, flip)
  else
  *)
    (TTextureManager.Instance()).DrawFrame(pr, textureId, round(position.x),
      round(position.y), w, h, currentRow, currentFrame, scale, angle, flip);
end;

procedure TGameObject.Update();
begin
  velocity := velocity + acceleration;
  position := position + velocity;
  Inc(animationCounter);
(*
  inc(x);
  if x > 600 then x := 0;
*)
end;

end.
