SRC = $(wildcard *.pas)
OBJ = $(SRC:%.pas=%.o)
PPU = $(SRC:%.pas=%.ppu)
EXE = main
$(EXE):$(OBJ)
	fpc -gl -gh -Fusdl2 -Fl. main.pas
%.o:%.pas
	fpc -gp -gl -gh -Fusdl2 -Fl. -B $^
.PHONY: clean
clean:
	rm -rf $(OBJ) $(PPU) $(EXE)
.PHONY: run
run: $(EXE)
	./$(EXE)

test: main.cpp
	g++ main.cpp -lsfml-graphics -lsfml-window -lsfml-system
